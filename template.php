<?php

/**
 * @file
 * This file provides theme override functions for the whfr2020 theme.
 */

/**
 * Implements hook_theme_preprocess_page().
 *
 * Override or set variables used by the page template.
 *
 * @param $variables
 *   A sequential array of variables to pass to the theme template.
 */
function whfr2020_preprocess_page(&$variables) {
  global $theme;

  // Add headers for responsive HTML5 output.
  $variables['head'] .= "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\" />\n";
  $variables['head'] .= "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n";

  // Add Drupal 7 style Generator tag.
  $variables['head'] .= "<meta name=\"Generator\" content=\"Drupal 6 (http://drupal.org)\" />\n";

  // Don't display empty help from node_help().
  if ($variables['help'] == "<div class=\"help\"><p></p>\n</div>") {
    $variables['help'] = '';
  }

  // Let's theme the primary and secondary links here, instead of calling theme() in page.tpl.php, ok?
  if ($variables['primary_links']) {
    $variables['themed_primary_links'] = theme('links', $variables['primary_links'], array('class' => 'links primary-links'));
  }
  if ($variables['secondary_links']) {
    $variables['themed_secondary_links'] = theme('links', $variables['secondary_links'], array('class' => 'links secondary-links'));
  }

  // Classes for body element. Allows advanced theming based on context.
  $body_classes = array($variables['body_classes']);
  if (user_access('administer blocks')) {
    $body_classes[] = 'admin';
  }

  // Add unique classes for each page and website section
  if (!$variables['is_front']) {
    $path = drupal_get_path_alias($_GET['q']);
    if (arg(0) == 'node' && arg(1) == 'add') {
      $body_classes[] = 'section-node-add';
    }
    elseif (arg(0)== 'node' && is_numeric(arg(1)) && (arg(2) == 'edit' || arg(2) == 'delete')) {
      $body_classes[] = 'section-node-'. arg(2); // Add 'section-node-edit' or 'section-node-delete'
    }
    else {
      list($section, ) = explode('/', $path, 2);
      $body_classes[] = whfr2020_id_safe('page-' . $path);
      $body_classes[] = whfr2020_id_safe('section-' . $section);
    }
  }
  $variables['body_classes'] = implode(' ', $body_classes); // Concatenate with spaces
}

/**
 * Implements hook_theme_preprocess_node().
 *
 * Override or insert variables used by the node template.
 *
 * This function creates the NODES classes, like 'node-unpublished' for nodes
 * that are not published, or 'node-mine' for node posted by the connected user...
 *
 * @param $variables
 *   A sequential array of variables to pass to the theme template.
 */
function whfr2020_preprocess_node(&$variables) {
  global $user;

  // Special classes for nodes
  $node_classes = array();
  if ($variables['sticky']) {
    $node_classes[] = 'sticky';
  }
  if (!$variables['node']->status) {
    $node_classes[] = 'node-unpublished';
    $variables['unpublished'] = TRUE;
  }
  else {
    $variables['unpublished'] = FALSE;
  }
  if ($variables['node']->uid && $variables['node']->uid == $user->uid) {
    // Node is authored by current user
    $node_classes[] = 'node-mine';
  }
  if ($variables['teaser']) {
    // Node is displayed as teaser
    $node_classes[] = 'node-teaser';
  }

  // Class for node type: "node-type-page", "node-type-story", "node-type-my-custom-type", etc.
  $node_classes[] = 'node-type-'. $variables['node']->type;
  $variables['node_classes'] = implode(' ', $node_classes); // Concatenate with spaces
}

/**
 * Create some custom classes for comments.
 *
 * @todo: This is called by comment.tpl.php and should be replaced with a preprocess function.
 */
function whfr2020_comment_classes($comment) {
  $node = node_load($comment->nid);
  global $user;

  $output .= ($comment->new) ? ' comment-new' : '';
  $output .=  ' '. $status .' ';
  if ($node->name == $comment->name) {
    $output .= 'node-author';
  }
  if ($user->name == $comment->name) {
    $output .=  ' mine';
  }
  return $output;
}

/**
 *
 * Implements hook_theme_menu_item_link().
 *
 * Customize the PRIMARY and SECONDARY LINKS, to allow the admin tabs to work on all browsers.
 *
 * @param $link
 *   array The menu item to render.
 * @return
 *   string The rendered menu item.
 */
function whfr2020_menu_item_link($link) {
  if (empty($link['options'])) {
    $link['options'] = array();
  }

  // If an item is a LOCAL TASK, render it as a tab
  if ($link['type'] & MENU_IS_LOCAL_TASK) {
    $link['title'] = '<span class="tab">'. check_plain($link['title']) .'</span>';
    $link['options']['html'] = TRUE;
  }

  if (empty($link['type'])) {
    $true = TRUE;
  }

  return l($link['title'], $link['href'], $link['options']);
}

/**
 * Implements hook_theme_menu_local_tasks().
 */
function whfr2020_menu_local_tasks() {
  $output = '';

  if ($primary = menu_primary_local_tasks()) {
    $output .= "<ul class=\"tabs primary clear-block\">\n". $primary ."</ul>\n";
  }
  if ($secondary = menu_secondary_local_tasks()) {
    $output .= "<ul class=\"tabs secondary clear-block\">\n". $secondary ."</ul>\n";
  }

  return $output;
}

/**
 * Implements hook_theme_menu_item().
 */
function whfr2020_menu_item($link, $has_children, $menu = '', $in_active_trail = FALSE, $extra_class = NULL) {
  $class = ($menu ? 'expanded' : ($has_children ? 'collapsed' : 'leaf'));
  if (!empty($extra_class)) {
    $class .= ' ' . $extra_class;
  }
  if ($in_active_trail) {
    $class .= ' active-trail';
  }
  $class .= ' ' . whfr2020_id_safe(str_replace(' ', '_', strip_tags($link)));
  return '<li class="'. $class . '">' . $link . $menu ."</li>\n";
}

/**
 * Converts a string to a suitable html ID attribute.
 *
 * http://www.w3.org/TR/html4/struct/global.html#h-7.5.2 specifies what makes a
 * valid ID attribute in HTML. This function:
 *
 *   - Ensure an ID starts with an alpha character by optionally adding an 'n'.
 *   - Replaces any character except A-Z, numbers, and underscores with dashes.
 *   - Converts entire string to lowercase.
 *
 * @param $string
 *   The string.
 * @return
 *   The converted string.
 */
function whfr2020_id_safe($string) {
  // Replace with dashes anything that isn't A-Z, numbers, dashes, or underscores.
  $string = drupal_strtolower(preg_replace('/[^a-zA-Z0-9_-]+/', '-', $string));
  // If the first character is not a-z, add 'n' in front.
  if (!ctype_lower($string{0})) { // Don't use ctype_alpha since its locale aware.
    $string = 'id'. $string;
  }
  return $string;
}

/**
 * Implements hook_theme_breadcrumb().
 */
function whfr2020_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    return '<div class="breadcrumb">'. implode(' &bull; ', $breadcrumb) .'</div>';
  }
}

/**
 * Implements hook_theme_username().
 *
 * Overrides default theme_username() function to display user profile full name
 * (if it exists) and remove "not verified" from anonymous comments.
 */
function whfr2020_username($object) {
  if (empty($object->profile_fullname)) {
    if ($object->uid && function_exists('profile_load_profile')) {
      profile_load_profile($object);
    }
  }

  if (!empty($object->profile_fullname)) {
    $name = $object->profile_fullname;
    if (user_access('access user profiles')) {
      return l($name, 'user/'. $object->uid, array('attributes' => array('title' => t('View user profile.'))));
    } else {
      return check_plain($name);
    }
  }

  // Profile field not set, default to standard behaviour

  if ($object->uid && $object->name) {
    // Shorten the name when it is too long or it will break many tables.
    if (drupal_strlen($object->name) > 20) {
      $name = drupal_substr($object->name, 0, 15) .'...';
    }
    else {
      $name = $object->name;
    }

    if (user_access('access user profiles')) {
      $output = l($name, 'user/'. $object->uid, array('attributes' => array('title' => t('View user profile.'))));
    }
    else {
      $output = check_plain($name);
    }
  }
  else if ($object->name) {
    // Sometimes modules display content composed by people who are
    // not registered members of the site (e.g. mailing list or news
    // aggregator modules). This clause enables modules to display
    // the true author of the content.
    if (!empty($object->homepage)) {
      $output = l($object->name, $object->homepage, array('attributes' => array('rel' => 'nofollow')));
    }
    else {
      $output = check_plain($object->name);
    }
    // $output .= ' ('. t('not verified') .')';
  }
  else {
    $output = variable_get('anonymous', t('Anonymous'));
  }
  return $output;
}
